#Build stage for the base system
FROM tensorflow/tensorflow:latest-gpu-py3 as base
COPY ./ /work/
WORKDIR /work/
RUN \
	pip3 install --upgrade pip &&\
	pip3 install -r requirements.txt &&\
	#Install the following packages to fix
	#the opencv-python installation error of the base image
	apt-get update &&\
	apt-get -y upgrade &&\
	apt-get install -y libsm6 libxext6 libxrender-dev

#Build stage for the development system
FROM base as devel
# Install Packages
RUN \
	apt-get update &&\
	apt-get -y upgrade &&\
	apt-get install -y\
	git\
	vim\
	# Python code formatter
	yapf3 &&\
	pip3 install --upgrade pip &&\
	# Python static code analysis tool
	pip3 install pylint

#Build stage for the release system
FROM base as release
RUN \
	pip3 install . &&\
	rm -rf /work/*
ENTRYPOINT ["n2dit"]
